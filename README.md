# Gitlab - Umbau der Example-Application
Baue bitte unsere 'example-k8s-application' für Gitlab um.
Das Frontend soll in einem S3 Bucket für Static Website Hosting laufen und das Backend auf einer EC2 Instanz (egal ob als Docker Container oder als NodeJS Prozess).
Erstelle die Infrastruktur bitte manuell.

Als Ergebnis sollte in deinem Gitlab Repo eine Pipeline laufen, die auf Git Tag, den aktuellen Stand der Anwendung bei AWS bereitstellt.

Bitte einen Link zu dem Gitlab Repo abgeben und einen Screenshot der laufenden Anwendung.

### 1. Schritt: Infrastruktur erstellen
- [ ] Frontend in einem S3 Bucket für Static Website Hosting
- [ ] Backend auf einer EC2 Instanz (egal ob als Docker Container oder als NodeJS Prozess)

###### Frontend in S3 Bucket
- [ ] S3 Bucket erstellen (Öffentlichen Zugriff zulassen, ACL aktivieren)
- [ ] Bucket für Static Website Hosting konfigurieren (unter Einstellungen, dann unter "Static Website Hosting")
- [ ] Das Frontend im ./frontend Ordner bauen mit `ng build`
- [ ] Das Frontend (dist-Ordner) in den S3 Bucket hochladen (Achtung: Bitte die einzelnen Dateien hochladen, nicht den Ordner, da wir die `index.html` als Website-Index verwenden)
- [ ] Bucket Policy erstellen, um den Zugriff auf die Website zu erlauben. Wir können dazu alle Dateien im Bucket anharken und auf Aktionen -> Veröffentlichen mit ACL drücken
- [ ] Die Website sollte nun unter der angezeigten Endpoint-URL (Eigenschaften -> Hosten einer statischen Website) erreichbar sein

###### Backend auf EC2
- [ ] EC2 Instanz erstellen (Amazon Linux) (mit Key-Pair - brauchen wir für SCP gleich)
- [ ] SSH und HTTP Ports öffnen, außerdem bitte den Port 3000 öffnen von überall
- [ ] NodeJS installieren (z.B. mit `sudo yum install -y nodejs`)
- [ ] Backend darauf kopieren (z.B. mit SCP: `scp -i <Pfad zum Key-Pair.pem> -r <Pfad zum Backend-Ordner> ec2-user@<public-ip>:~`)
- [ ] Backend starten (z.B. mit `npm install` und `npm start`)
- [ ] Backend sollte nun unter der Public-IP-Adresse und Port 3000 erreichbar sein

###### Frontend im S3 Bucket anpassen
- [ ] Die URL des Backends in der `environment.ts` Datei anpassen (z.B. `http://<public-ip der EC2-Instanz>:3000`)
- [ ] Das Frontend neu bauen mit `ng build`
- [ ] Das Frontend (dist-Ordner) in den S3 Bucket hochladen (Achtung: Bitte die einzelnen Dateien hochladen, nicht den Ordner, da wir die `index.html` als Website-Index verwenden)
- [ ] Die Dateien sollten nun wieder per ACL freigegeben werden (siehe Punkt oben)

### 2. Schritt: Gitlab Pipeline erstellen
- [ ] Gitlab Repo erstellen (Importieren des Github Repos)
- [ ] Gitlab Pipeline erstellen (`.gitlab-ci.yml` Datei im Root-Verzeichnis des Repos)
- [ ] Testen, ob wir den aws-cli-Job nutzen können. Es gibt jetzt ein Docker-Image mit aws-cli, das wir nutzen können: Achtung, wir müssen die aws-credentials als Environment-Variablen setzen (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_SESSION_TOKEN, AWS_BUCKET_NAME, AWS_DEFAULT_REGION)
```
upload to s3:
  image:
    name: amazon/aws-cli
    entrypoint: [""]
  script:
    - aws --version
```
- [ ] Pipeline so anpassen, dass wir vorher einen build-step des Frontends machen, und dann den dist-Folder in den S3 Bucket hochladen. Achtung, der dist-Folder muss dann als Artefakt gespeichert werden, damit wir ihn in einem späteren Schritt wieder verwenden können
```
build:
  stage: build
  image: node:latest
  script:
    - cd frontend  
    - npm install  
    - npm run build
    - ls -la  
  artifacts:
    paths:
      - frontend/dist
```
- [ ] Pipeline so anpassen, dass wir den dist-Folder aus den Artefakten laden und in den S3 Bucket hochladen
```
upload to s3:
  stage: deploy
  needs:
    - build
  image:
    name: amazon/aws-cli
    entrypoint: [""]
  script:
    - aws --version
    - cd frontend/dist/frontend/browser
    - ls -la
    - aws s3 cp . s3://$AWS_S3_BUCKET/ --recursive
```
- [ ] Pipeline so anpassen, dass wir den Backend-Ordner auf die EC2-Instanz kopieren und das Backend starten
```
ansible-to-ec2:
  stage: deploy-backend
  image: williamyeh/ansible:alpine3
  before_script:
    - echo "$PRIVATE_KEY" > ./temp_key.pem
    - chmod 600 ./temp_key.pem
    - export ANSIBLE_HOST_KEY_CHECKING=False
  script:
    - ansible --version
    - ansible-playbook -i hosts.ini deploy_backend.yml --private-key=./temp_key.pem
    - rm -f ./temp_key.pem
```
- [ ] Den SSH-Key für die EC2-Instanz in eine Variable PRIVATE_KEY hinterlegen. Wichtig zum Kopieren bitte `cat <Pfad zum Key-Pair>| pbcopy` verwenden, damit keine Zeilenumbrüche entstehen
- [ ] Die `hosts.ini` Datei anlegen und die Public-IP der EC2-Instanz hinterlegen
```
[backend-servers]
<public-ip der EC2-Instanz> ansible_user=ec2-user 
```
- [ ] Das `deploy_backend.yml` Playbook anlegen, das den Backend-Ordner kopiert und das Backend startet
```
- hosts: all
  become: yes
  tasks:
    - name: Kopiere den Backend-Ordner auf die EC2-Instanz
      synchronize:
        src: ./backend
        dest: ~/backend-ci
        recursive: yes
```
- [ ] Das Backend soll nun auf der EC2-Instanz laufen. Dazu müssen wir die Abhängigkeiten installieren und das Backend starten
```
- hosts: all
  become: yes
  tasks:
    - name: Kopiere den Backend-Ordner auf die EC2-Instanz
      synchronize:
        src: ./backend
        dest: /home/ec2-user/backend-ec2  
        recursive: yes

    - name: Führe npm install im Backend-Ordner aus
      shell: npm install
      args:
        chdir: /home/ec2-user/backend-ec2/backend

    - name: Starte die Anwendung mit npm run start
      shell: npm run start
      args:
        chdir: /home/ec2-user/backend-ec2/backend
```
Frontend über "Frontend: http://$AWS_S3_BUCKET.s3-website.$AWS_DEFAULT_REGION.amazonaws.com" erreichbar
###### Umbau auf Tags
- [ ] Pipeline so anpassen, dass sie nur auf Tags läuft
```
  rules:
        - if: '$CI_COMMIT_TAG'
```
unten an jeden Job anhängen

## Alternative mit Docker Images und Ansible zum Starten des Docker Containers
- [ ] Dockerfile für das Backend erstellen
- [ ] Docker Image bauen und in Docker Hub pushen
- [ ] Pipeline so anpassen, dass wir das Docker Image auf die EC2-Instanz kopieren und starten
```
build-image:
  stage: build-backend
  image: docker:26.1.0
  services:
    - docker:19.03.12-dind
  variables:
        DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    - echo $DOCKER_TOKEN | docker login -u $DOCKER_USERNAME --password-stdin
  script:
    - docker build -t $IMAGE_NAME:$IMAGE_TAG ./backend
    - docker images
    - docker push $IMAGE_NAME:$IMAGE_TAG
  rules:
    - if: '$CI_COMMIT_TAG'
```
Achtung: Wir müssen die Umgebungsvariablen DOCKER_USERNAME, DOCKER_TOKEN, IMAGE_NAME und IMAGE_TAG setzen. IMAGE_TAG wird auf den gezogenen Tag gesetzt. Der rest muss gefüllt werden. 
- [ ] Ansible Playbook anpassen, dass der Docker Container gestartet wird
```
- hosts: all
  become: yes
  tasks:
    - name: Install Docker
      yum:
        name: docker
        state: present

    - name: Start Docker service
      service:
        name: docker
        state: started
        enabled: yes

    - name: Run Docker container
      shell: >
        docker run -d -p 3000:3000 helenhaveloh/k8s-application
      args:
        executable: /bin/bash

```